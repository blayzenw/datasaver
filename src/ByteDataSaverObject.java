/**
 * Created by Blayze on 5/3/2017.
 */
public abstract interface ByteDataSaverObject {
    //Interfaces must also have a empty constructor
    int dataVersionID();
    void save(DataSaverWriter saver, int dataVersionID);
    void load(DataSaverReader saver, int dataVersionID);
}
