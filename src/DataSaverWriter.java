import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import java.util.zip.Deflater;

/**
 * @author Blayze
 *
 * When writing objects and lists, the size of objects/list is unknown until everything in the scope has been written.
 * Due to the integer/long optimization we are unable to write everything in place using a set size block + data
 * because the size of a int/long changes based on its value. The trade off of a slower write (extra object creation
 * and data transfer) using another DataSaverWriter as a scope was made in favor of smaller data size.
 *
 */

public class DataSaverWriter {
    //Masks
    static final int EMPTY = 32;
    static final int NEGATIVE = 128;
    static final int SIZE = 15;

    //Minimum and maximum value that can be written into a single byte using the int/long optimization
    static final int MIN_SINGLE_BYTE_SIZE = -31;
    static final int MAX_SINGLE_BYTE_SIZE = 31;

    //The storage for all of the data
    private final ByteArrayOutputStream stream;

    public DataSaverWriter(){
        stream = new ByteArrayOutputStream();
    }


    /**
     * @return A new DataSaverReader with all of the data that has been written to the saver
     */
    public DataSaverReader getReader(){
        return new DataSaverReader(stream.toByteArray());
    }

    public byte[] getData(){
        return stream.toByteArray();
    }

    /**
     * The order in which a value is stored and retrieved needs to be the same.
     *
     * @param value A String to save.
     */
    public void write(String value) {
        writeString(value);
    }

    /**
     * The order in which a value is stored and retrieved needs to be the same.
     *
     * @param value A int to save.
     */
    public void write(int value){
        writeBytes(toBytes((long) value));
    }

    /**
     * The order in which a value is stored and retrieved needs to be the same.
     *
     * @param value A long to save.
     */
    public void write(long value){
        writeBytes(toBytes(value));
    }

    /**
     * The order in which a value is stored and retrieved needs to be the same.
     *
     * @param value A float to save.
     */
    public void write(float value){
        writeBytes(toBytes(value));
    }


    /**
     * The order in which a value is stored and retrieved needs to be the same.
     *
     * @param value A double to save.
     */
    public void write(double value){
        writeBytes(toBytes(value));
    }

    /**
     * The order in which a value is stored and retrieved needs to be the same.
     *
     * @param value A byte to save.
     */
    public void write(byte value){
        writeBytes(new byte[]{value});
    }


    /**
     *
     * This is different from write bytes as it will also record the size of
     * the bytes written for use with readBytes from the reader.
     *
     * @param value A byte array to write to the buffer.
     */
    public void write(byte[] value){
        int size = value.length;
        write(size);
        writeBytes(value);
    }


    /**
     * Boolean values are stored in a single byte with a value of 0 or 1
     *
     * @param value The boolean value to write
     */
    public void write(boolean value){
        if(value){
            writeBytes(new byte[]{1});
        }
        else {
            writeBytes(new byte[]{0});
        }
    }


    /**
     * Same functionality of the write functions but the name is
     * changed to avoid naming conflicts
     *
     * @param items A list of Strings to write
     */
    public void writeStringArray(List<String> items){
        DataSaverWriter scope = new DataSaverWriter();
        for(String item : items){
            scope.write(item);
        }
        byte[] data = scope.getData();
        write(data.length);
        writeBytes(data);
    }


    /**
     * ByteDataSaverObject objects are written in the format of
     * [int/long size of the object's bytes][object's bytes]
     *
     * @param item A object that implements the ByteDataSaverObject interface
     */
    public void write(ByteDataSaverObject item){
        if(item == null){
            return;
        }
        int versionId = item.dataVersionID();

        DataSaverWriter scope = new DataSaverWriter();
        item.save(scope, versionId);

        byte[] scope_data = scope.getData();
        int dataSize = scope_data.length;

        write(versionId);
        write(dataSize);
        writeBytes(scope_data);
    }


    /**
     * ByteDataSaverObject lists are written in the format of
     * [long/int size of the all object's byte data][byte data of all objects]
     *
     * @param items A list of ByteDataSaverObjects to write
     * @param <T> A object that implements the ByteDataSaverObject interface
     */
    public <T extends ByteDataSaverObject> void write(List<T> items){
        DataSaverWriter scope = new DataSaverWriter();
        for(T item : items){
            scope.write(item);
        }
        byte[] data = scope.getData();
        write(data.length);
        writeBytes(data);
    }

    /**
     * String are written in the format of
     * [long/int size of the string bytes][string bytes]
     *
     * @param data The string to write to the buffer
     */
    private void writeString(String data){
        byte[] byteData = data.getBytes();
        byte[] dataSize = toBytes(byteData.length);
        writeBytes(dataSize);
        writeBytes(byteData);
    }


    /**
     * Writes bytes directly to the buffer
     *
     * @param data The data to write to the buffer
     */
    private void writeBytes(byte[] data){
        try {
            stream.write(data);
        }
        catch (IOException e){
            //The stream should never close so this should not happen
            throw new RuntimeException(e);
        }
    }

    /**
     * Floating point values are more complex than int/long values
     * and the optimization of removing the starting zero value bytes
     * can not be done without increasing the size of the data so these
     * values are written and read in the raw format. There is no need
     * for the size to be written because a double will aways be 4 bytes.
     *
     * @param input The value to convert into bytes
     * @return The byte array representation of a float
     */
    private static byte[] toBytes(float input){
        return ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putFloat(input).array();
    }

    /**
     * Floating point values are more complex than int/long values
     * and the optimization of removing the starting zero value bytes
     * can not be done without increasing the size of the data so these
     * values are written and read in the raw format. There is no need
     * for the size to be written because a double will aways be 8 bytes.
     *
     * @param input The value to convert into bytes
     * @return The byte array representation of a double
     */
    private static byte[] toBytes(double input){
        return ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN).putDouble(input).array();
    }


    /**
     * This will attempt to reduce the number of bytes used for storing a long
     * by removing all leading zero value bytes. This is done by breaking apart the
     * long value into two parts, a header and the data part. The header stores information
     * such as if the value is negative, empty (value stored within the header) and the
     * number of bytes used. The negative bit is put into the header to un-flip the bits from the
     * negative representation.
     * <p>
     * If the value is between <code>MIN_SINGLE_BYTE_SIZE</code> and <code>MAX_SINGLE_BYTE_SIZE</code> then the value can
     * be put into the header and the total space used will be one byte.
     * <p>
     * If the value is greater/smaller than that, the right most 4 bits will represent the number of
     * bytes used to represent the data.
     *
     * <p>This optimization only works well for small values.
     * <p>-31 to 31 -> 1 byte
     * <p>-255 to -32 and 32 to 255 -> 2 bytes
     * <p>-65535 to -256 and 256 to 65535 -> 3 bytes
     * <p>-167772015 to -65535 and 65535 to 167772015 -> 4 bytes
     * <p>Everything else -> 5 bytes
     *
     * @param input A long value to store
     * @return A byte array representing the long value in a optimized format
     */
    private static byte[] toBytes(long input){
        int mask = 0;

        //If the value can be put into a single byte
        if(input <= MAX_SINGLE_BYTE_SIZE && input >= MIN_SINGLE_BYTE_SIZE){
            byte[] data = new byte[1];
            data[0] = (byte) input;
            data[0] = (byte) (data[0] | EMPTY);
            if(input < 0){
                data[0] = (byte) (data[0] | NEGATIVE);
            }
            return data;
        }

        //If negative, set the negative mask and convert to positive for optimizations
        if(input < 0){
            input *= -1;
            mask = mask | NEGATIVE;
        }

        //Get bytes by part
        byte b0 = (byte) ((input & 0xFF00000000000000L) >> 56);
        byte b1 = (byte) ((input & 0x00FF000000000000L) >> 48);
        byte b2 = (byte) ((input & 0x0000FF0000000000L) >> 40);
        byte b3 = (byte) ((input & 0x000000FF00000000L) >> 32);
        byte b4 = (byte) ((input & 0x00000000FF000000L) >> 24);
        byte b5 = (byte) ((input & 0x0000000000FF0000L) >> 16);
        byte b6 = (byte) ((input & 0x000000000000FF00L) >> 8);
        byte b7 = (byte) ((input & 0x00000000000000FFL));

        //Create a byte array that excludes left side zero value bytes
        byte[] data;
        if(b0 != 0){
            data = new byte[9];
            data[1] = b0;
            data[2] = b1;
            data[3] = b2;
            data[4] = b3;
            data[5] = b4;
            data[6] = b5;
            data[7] = b6;
            data[8] = b7;
        }
        else if(b1 != 0){
            data = new byte[8];
            data[1] = b1;
            data[2] = b2;
            data[3] = b3;
            data[4] = b4;
            data[5] = b5;
            data[6] = b6;
            data[7] = b7;
        }
        else if(b2 != 0){
            data = new byte[7];
            data[1] = b2;
            data[2] = b3;
            data[3] = b4;
            data[4] = b5;
            data[5] = b6;
            data[6] = b7;
        }
        else if(b3 != 0){
            data = new byte[6];
            data[1] = b3;
            data[2] = b4;
            data[3] = b5;
            data[4] = b6;
            data[5] = b7;
        }
        else if(b4 != 0){
            data = new byte[5];
            data[1] = b4;
            data[2] = b5;
            data[3] = b6;
            data[4] = b7;
        }
        else if(b5 != 0){
            data = new byte[4];
            data[1] = b5;
            data[2] = b6;
            data[3] = b7;
        }
        else if(b6 != 0){
            data = new byte[3];
            data[1] = b6;
            data[2] = b7;
        }
        else {
            data = new byte[2];
            data[1] = b7;
        }
        //Store the number of bytes used
        mask = mask + data.length-1;
        data[0] = (byte) mask;
        return data;
    }

    /**
     * @return A compressed version of the data using Java's Deflater if there is data otherwise it will
     * return a zero size byte array
     */
    public byte[] getCompressed(){
        if(getData().length == 0){
            return getData();
        }

        byte[] output = null;
        try {
            byte[] data = getData();
            Deflater deflater = new Deflater();
            deflater.setInput(data);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

            deflater.finish();
            byte[] buffer = new byte[1024];
            while (!deflater.finished()) {
                int count = deflater.deflate(buffer); // returns the generated code... index
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
            output = outputStream.toByteArray();

            deflater.end();
        }
        catch (IOException e){
            //The stream should never close so this should not happen
            throw new RuntimeException(e);
        }
        return output;
    }

}
