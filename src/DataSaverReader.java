import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Stack;
import java.util.zip.Inflater;

/**
 * @author Blayze
 *
 */
public class DataSaverReader {
    private ByteArrayInputStream inputStream;
    private int size;
    private int endMark;
    private int position;
    private Stack<Integer> scope;

    /**
     * @param data Uncompressed data from DataSaverWriter
     */
    public DataSaverReader(byte[] data){
        if(data == null){
            data = new byte[0];
        }
        inputStream = new ByteArrayInputStream(data);
        scope = new Stack<>();
        size = data.length;
        endMark = size;
        position = 0;
    }


    /**
     * This will copy the data from the <code>inputStream</code> if there is data
     * left in the scope defined by <code>position</code> and <code>endMark</code>.
     * If there is no data within that range, nothing will be copied over.
     *
     * @param copyTo The byte array to copy the data to
     */
    private void readBytes(byte[] copyTo){
        try {
            int to;
            if(position + copyTo.length > endMark){
                to = endMark - position;
            }
            else {
                to = copyTo.length;
            }
            position += to;
            inputStream.read(copyTo, 0, to);
        }
        catch (NullPointerException|IndexOutOfBoundsException e){
            //This should already be checked for and prevented
            throw new RuntimeException(e);
        }
    }


    /**
     * @return The number of bytes left that are available to be read within the scope.
     */
    private int bytesLeft(){
        return endMark -position < 0 ? 0 : endMark -position;
    }


    /**
     * This is used to limit the scope of that can be read. It is used
     * when reading arrays of objects where the number of items is unknown but
     * the size of the data is known.
     *
     * @param limit The limit for the number of bytes to read from the current position
     */
    private void limitBytes(int limit){
        scope.push(endMark);
        endMark = position + limit;
    }


    /**
     * Resets the end limit to what it was before it was limited provided that <code>limitBytes</code> was called.
     */
    private void resetLimit(){
        if(scope.size() > 0){
            endMark = scope.pop();
        }
        else {
            throw new RuntimeException("Empty stack");
        }
    }


    /**
     * The order in which a value is read needs to be the same order in which it was stored.
     *
     * @return The saved string, empty string if there is no data
     */
    public String readString(){
        byte marker = getMarker();
        int dataSize = readIntFromBuffer(marker);
        byte[] data = new byte[dataSize];
        readBytes(data);

        return new String(data);
    }


    /**
     * The order in which a value is read needs to be the same order in which it was stored.
     *
     * @return The saved int, 0 if there is no data
     */
    public int readInteger(){
        return readIntFromBuffer(getMarker());
    }


    /**
     * The order in which a value is read needs to be the same order in which it was stored.
     *
     * @return The saved long, 0 if there is no data
     */
    public long readLong(){
        return readLongFromBuffer(getMarker());
    }

    /**
     * The order in which a value is read needs to be the same order in which it was stored.
     *
     * @return The saved float, 0 if there is no data
     */
    public float readFloat(){
        byte[] data = new byte[4];
        readBytes(data);
        return ByteBuffer.wrap(data).order(ByteOrder.BIG_ENDIAN).getFloat();
    }

    /**
     * The order in which a value is read needs to be the same order in which it was stored.
     *
     * @return The saved double, 0 if there is no data
     */
    public double readDouble(){
        byte[] data = new byte[8];
        readBytes(data);
        return ByteBuffer.wrap(data).order(ByteOrder.BIG_ENDIAN).getDouble();
    }

    /**
     * The order in which a value is read needs to be the same order in which it was stored.
     *
     * @return The saved byte, 0 if there is no data
     */
    public byte readByte(){
        byte[] data = new byte[1];
        readBytes(data);
        return data[0];
    }

    /**
     * The order in which a value is read needs to be the same order in which it was stored.
     *
     * @return The saved byte array, byte array of size 0 if there is no data
     */
    public byte[] readBytes(){
        int size = readInteger();
        byte[] data = new byte[size];
        readBytes(data);
        return data;
    }

    /**
     * The order in which a value is read needs to be the same order in which it was stored.
     *
     * @return The saved boolean, false if there was no data
     */
    public boolean readBoolean(){
        byte[] data = new byte[1];
        readBytes(data);
        if(data[0] == 1){
            return true;
        }
        else if(data[0] == 0){
            return false;
        }
        else {
            System.err.println("Error incorrect boolean value found, returning false");
            return false;
        }
    }

    /**
     * The order in which a value is read needs to be the same order in which it was stored.
     *
     * @return The saved String array, empty array if there is no data
     */
    public ArrayList<String> readStringArray(){
        ArrayList<String> items = new ArrayList<>();
        int size = readInteger();
        limitBytes(size);
        while (bytesLeft() > 0){
            items.add(readString());
        }
        resetLimit();
        return items;
    }

    /**
     * The order in which a value is read needs to be the same order in which it was stored.
     *
     * @return The the saved array of objects that implement the ByteDataSaverObject interface, empty array if there
     * is no data
     */
    public <T extends ByteDataSaverObject> ArrayList<T> readObjArray(Class<T> itemClass){
        ArrayList<T> items = new ArrayList<>();

        int size = readInteger();
        limitBytes(size);
        while(bytesLeft() > 0){
            items.add(readObj(itemClass));
        }
        resetLimit();
        return items;
    }

    /**
     * @param itemClass The class of the object to read. This must implement ByteDataSaverObject and have a empty
     *                   constructor or a runtime exception will be thrown.
     * @param <T> A class that implements ByteDataSaverObject
     * @return The loaded object
     */
    public <T extends ByteDataSaverObject> T readObj(Class<T> itemClass){
        T object = null;

        //ByteDataSaverObject must implement a empty constructor for newInstance to work
        try {
            object = itemClass.newInstance();
        }
        catch (Exception e){
            throw new RuntimeException(e);
        }

        if(object == null){
            return null;
        }

        int versionId = readInteger();
        int dataSize = readInteger();

        limitBytes(dataSize);
        object.load(this, versionId);
        resetLimit();

        return object;
    }


    /**
     * The marker/header contains information on the data such as the value or if it is
     * negative or the number of bytes to read.
     * @return The marker/header of the data
     */
    private byte getMarker(){
        byte[] marker = new byte[1];
        readBytes(marker);
        return marker[0];
    }


    /**
     * @param marker The marker/header of the data
     * @return The int value of the data
     */
    private int readIntFromBuffer(byte marker){
        return (int) readLongFromBuffer(marker);
    }


    /**
     * See DataSaverWriter write long for more information on the format
     *
     * @param marker The marker/header of the data
     * @return The long value of the data
     */
    private long readLongFromBuffer(byte marker){
        if (isMasked(marker, DataSaverWriter.EMPTY)) {
            byte mask = 31;
            long value = marker & mask;
            if(marker < 0){
                value *= -1;
            }
            return value;
        }

        boolean neg = isMasked(marker, DataSaverWriter.NEGATIVE);
        int size = marker & DataSaverWriter.SIZE;

        byte[] sizeData = new byte[size];
        readBytes(sizeData);

        long value = 0;
        for(int i = 0; i < size; i++){
            /*
            Java doesn't allow unsigned operations or casting so the sign bit needs to be removed,
            convert to a long and then added back in the bit to the long.
            127 -> 01111111
            128 -> 10000000
            */
            long convertedValue = sizeData[i] & 127;
            if(sizeData[i] < 0) {
                convertedValue = convertedValue | 128;
            }
            value = value | (convertedValue << (8*(sizeData.length - i - 1)));
        }

        if(neg){
            value *= -1;
        }

        return value;
    }

    /**
     * @param data The data to check if a mask/bit is set
     * @param mask The mask/bit to check
     * @return True if the bit of the mask is active, false if its not
     */
    private static boolean isMasked(int data, int mask) {
        return (data & mask) == mask;
    }

    /**
     * @param data The compressed data from DataSaverReader
     * @return The uncompressed data, or a empty byte array if there was no data
     */
    public static DataSaverReader readCompressed(byte[] data){
        if(!hasData(data)){
            return new DataSaverReader(data);
        }


        byte[] output = null;
        try {
            Inflater inflater = new Inflater();
            inflater.setInput(data);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
            byte[] buffer = new byte[1024];
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
            output = outputStream.toByteArray();

            inflater.end();
        }
        catch (Exception e){
            throw new RuntimeException(e);
        }
        return new DataSaverReader(output);
    }


    /**
     * @param data Data from a DataSaverWriter
     * @return True if there is any data that the DataSaverReader can read. False if there is none.
     */
    public static boolean hasData(byte[] data){
        if(data == null || data.length == 0){
            return false;
        }
        return true;
    }
}
