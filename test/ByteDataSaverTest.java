import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

/**
 * Created by Blayze on 5/2/2017.
 */
public class ByteDataSaverTest {
    //Test compression of largeish amount of data
    @Test
    public void testCompression(){
        Random r = new Random();
        r.setSeed(1);

        int items = 5000000;

        DataSaverWriter saver = new DataSaverWriter();
        for(int i = 0; i < items; i++){
            saver.write(r.nextInt());
        }

        long start = System.currentTimeMillis();
        byte[] compressedData = saver.getCompressed();
        long end = System.currentTimeMillis();
        System.out.println("Compression of " + items + " items took " + (end-start) + "ms");

        start = System.currentTimeMillis();
        DataSaverReader reader = DataSaverReader.readCompressed(compressedData);
        end = System.currentTimeMillis();
        System.out.println("Decompression of " + items + " items took " + (end-start) + "ms");

        r.setSeed(1);
        for(int i = 0; i < items; i++){
            assertTrue(r.nextInt() == reader.readInteger());
        }
    }


    //Test random ints
    @Test
    public void testIntegerFussy(){
        Random r = new Random();
        r.setSeed(1);

        DataSaverWriter saver = new DataSaverWriter();
        for(int i = 0; i < 1000000; i++){
            saver.write(r.nextInt());
        }

        DataSaverReader reader = saver.getReader();
        r.setSeed(1);
        for(int i = 0; i < 1000000; i++){
            assertTrue(r.nextInt() == reader.readInteger());
        }
    }

    //Test random longs
    @Test
    public void testLongFussy(){
        Random r = new Random();
        r.setSeed(1);

        DataSaverWriter saver = new DataSaverWriter();
        for(int i = 0; i < 1000000; i++){
            saver.write(r.nextLong());
        }

        DataSaverReader reader = saver.getReader();
        r.setSeed(1);
        for(int i = 0; i < 1000000; i++){
            assertTrue(r.nextLong() == reader.readLong());
        }
    }

    //Verify saving and reading a byte works
    @Test
    public void byteTest(){
        byte v = -32;
        DataSaverWriter saver = new DataSaverWriter();
        saver.write(v);

        DataSaverReader reader = saver.getReader();
        assertTrue(v == reader.readByte());
    }

    //Verify saving and reading a boolean works
    @Test
    public void testBoolean(){
        boolean v = true;
        boolean v1 = false;
        DataSaverWriter saver = new DataSaverWriter();
        saver.write(v);
        saver.write(v1);

        DataSaverReader reader = saver.getReader();
        assertTrue(reader.readBoolean() == v);
        assertTrue(reader.readBoolean() == v1);
    }

    //Verify saving and reading a string array works
    @Test
    public void testStringArray(){
        ArrayList<String> data = new ArrayList<>();
        for(int i = 0; i < 1000; i++){
            data.add(i + "asd");
        }
        DataSaverWriter saver = new DataSaverWriter();
        saver.writeStringArray(data);

        DataSaverReader reader = saver.getReader();
        ArrayList<String> readData = reader.readStringArray();
        for(int i = 0; i < 1000; i++){
            assertTrue(readData.get(i).equals(data.get(i)));
        }
    }

    //Verify saving and reading a single integer works
    @Test
    public void testInteger(){
        int v = 10;

        DataSaverWriter saver = new DataSaverWriter();
        saver.write(v);

        DataSaverReader reader = saver.getReader();
        int vr = reader.readInteger();
        assertTrue(v == vr);
    }

    //Verify saving and reading two string in a row works
    @Test
    public void testReadString() {
        String test1 = "asdf34r2d";
        String test2 = "dfgerg";
        DataSaverWriter saver = new DataSaverWriter();
        saver.write(test1);
        saver.write(test2);

        DataSaverReader reader = saver.getReader();
        assertTrue(reader.readString().equals(test1));
        assertTrue(reader.readString().equals(test2));
    }

    //Verify saving and reading a large long works
    @Test
    public void testLong(){
        long v = -133451234345L;

        DataSaverWriter saver = new DataSaverWriter();
        saver.write(v);

        DataSaverReader reader = saver.getReader();
        long vr = reader.readLong();

        assertTrue(v == vr);
    }

    //Verify saving and reading a float works
    @Test
    public void testFloat(){
        float v = 132.3452f;

        DataSaverWriter saver = new DataSaverWriter();
        saver.write(v);

        DataSaverReader reader = saver.getReader();
        float vr = reader.readFloat();

        assertTrue(v == vr);
    }

    //Verify saving and reading a double works
    @Test
    public void testDouble(){
        double v = 1232.3452d;

        DataSaverWriter saver = new DataSaverWriter();
        saver.write(v);

        DataSaverReader reader = saver.getReader();
        double vr = reader.readDouble();

        assertTrue(v == vr);
    }

    //Verify saving and reading a object works
    @Test
    public void testObject(){
        NewDataSaverObjectTest object = new NewDataSaverObjectTest("asd23", 4);

        DataSaverWriter writer = new DataSaverWriter();
        writer.write(object);

        DataSaverReader reader = writer.getReader();
        NewDataSaverObjectTest read = reader.readObj(NewDataSaverObjectTest.class);
        assertTrue(object.equals(read));
    }

    //Verify saving and reading a object array works
    @Test
    public void testObjectArr(){
        int count = 20;
        ArrayList<NewDataSaverObjectTest> items = new ArrayList<>();
        for(int i = 0; i < count; i++){
            items.add(new NewDataSaverObjectTest(i+"", i+1));
        }
        DataSaverWriter writer = new DataSaverWriter();
        writer.write(items);

        DataSaverReader reader = writer.getReader();
        ArrayList<NewDataSaverObjectTest> readItems = reader.readObjArray(NewDataSaverObjectTest.class);
        for(int i = 0; i < count; i++){
            assertTrue(items.get(i).equals(readItems.get(i)));
        }
    }

    //Make sure that reading data past what exists will not cause errors or incorrectly read the wrong data
    @Test
    public void testReadingEmptyData(){
        String v = "asd ";
        DataSaverWriter writer = new DataSaverWriter();
        writer.write(v);

        DataSaverReader reader = writer.getReader();
        assertTrue(reader.readString().equals(v));
        assertTrue(reader.readString().equals(""));
        assertTrue(reader.readInteger() == 0);
    }

}