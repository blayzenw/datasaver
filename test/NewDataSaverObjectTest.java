/**
 * Created by Blayze on 5/1/2017.
 */
public class NewDataSaverObjectTest implements ByteDataSaverObject {
    String a;
    String b;
    int c;
    NewDataSaverObjectTest obj;
    //ArrayList<String> strings;
    protected NewDataSaverObjectTest(){}
    public NewDataSaverObjectTest(String a, int sub_items){
        this.a = a;
        this.b = "324564";
        this.c = 1234;
    }
    public int dataVersionID(){
        return 1;
    }
    public void save(DataSaverWriter saver, int version){
        saver.write(a);
        saver.write(b);
        saver.write(c);
    }
    public void load(DataSaverReader saver, int version){
        a = saver.readString();
        b = saver.readString();
        c = saver.readInteger();
    }

    @Override
    public boolean equals(Object o){
        if(!(o instanceof NewDataSaverObjectTest)){
            return false;
        }

        NewDataSaverObjectTest temp = (NewDataSaverObjectTest) o;
        if(!a.equals(temp.a)){
            return false;
        }
        if(!b.equals(temp.b)){
            return false;
        }
        if(c != temp.c){
            return false;
        }

        return true;
    }
}
