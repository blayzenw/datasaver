## DataSaver ##

DataSaver is a lightweight object serialization package for quickly reading and writing to and from a byte array in a compressed format.

 
## Goal ##
The goal of this project is to save large data structure objects into a byte array with the least amount of data possible. This is unlike Java Serialization which saves extra information such as the class and field names to ensure compatibility between reading and writing. The list of trade-offs that were made are listed below.


### Interface ###
Objects must implement the ByteDataSaverObject interface which requires save, load and dataVersionID functions to be implemented. In addition, they must also implement a empty constructor so the classes can be recreated and repopulated with data.

### Same Order Save and Load ###
Class, variable or key information is not stored so data must be saved and loaded in the same order. New variables to be saved can be appended without problems, however the order can not change within a version.

### Class Data Version ID ###
Each class is required to provide a version ID. This is used for backward compatibility between upgrading the data. This is the only class information that is saved.


## Caveats ##
Some of the optimizations done rely on data values or sizes being smallish. Integer values that are over 167,772,015 or under -167,772,015 will use up 5 bytes. However values between -31 and 31 will only use up 1 byte. See Integer/Long Space Optimization below for more information.


## Data Format ##
The following is a list on how the data is stored for each type.

### Integer/Long Space Optimization ####
Integers and Longs are stored in a special format that tries to save space by removing all of the left most zero value bytes. For example, take the int value 10 where he first 28 bits are all 0. The value can be more efficiently in a single byte were only the first 4 bits would be all wasted. By limiting the bytes to only what is necessary and having a header containing the number of bytes used, sign, Longs and Integers can be stored in a much smaller format. At most the size of a long can be reduced by 87.5% and Integers by 75%. However this only words if the numbers are small, see the values below for sizes. The sign bit is moved to the header due to the bits being flipped with negative signed values.

* -31 to 31 -> 1 byte
* 255 to -32 and 32 to 255 -> 2 bytes
* -65,535 to -256 and 256 to 65,535 -> 3 bytes
* -167,772,015 to -65,535 and 65,535 to 167,772,015 -> 4 bytes
* -2,147,483,647 to -167,772,015 and 167,772,015 to -2,147,483,647 -> 5 bytes


The format of the header is the following.

* X0000000 -> Sign bit (positive/negative)
* 0X000000 -> Empty bit if the value is stored in the left 6 most bits
* 00XXXXXX -> If the empty bit is set then its the value. If the empty bit is not set then its the number of bytes. 


### Further Compression ####
The data can be further compressed if the data is compressed using getCompressed() and readCompressed(). These function uses java.util.zip package if there is data, otherwise is will return a empty byte array.

### Compression Examples ###

10,000 random longs between 0 and 50,000

* Standard = 80,000 bytes (100%)
* DataSaver (normal) = 29,943 bytes (37%)
* DataSaver (with zip) = 23,553 bytes (29%)


10,000 random longs between 0 and 255

* Standard = 80,000 bytes (100%)
* DataSaver (normal) = 18,703 bytes (23%)
* DataSaver (with zip) = 12,921 bytes (16%)


10,000 random ints between 0 and 255

* Standard = 40,000 bytes (100%)
* DataSaver (normal) = 18,703 bytes (47%)
* DataSaver (with zip) = 12,921 bytes (32%)